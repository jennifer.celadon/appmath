import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth-service';
import { User } from '../models/User';
// import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  formRegister: FormGroup;
  loginInvalid: boolean;
  formSubmitAttempt: boolean;
  returnUrl: string;
  isLogin = true;
  usernameExist  = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    // private toastr: ToastrService,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit() {

    this.formLogin = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });


    this.formRegister = this.fb.group({
      username: ['', Validators.required],
      name: ['', Validators.required], 
      surname: ['', Validators.required],  
      score: [0],         
      password: ['', Validators.required]
    });

    if ( this.authService.user ) {
      this.router.navigate(["mcd"]);
    }
  }
  toggleTab() {
    this.isLogin = !this.isLogin;
  }

  onLogin() {    
    const user : User = this.formLogin.value;
    this.authService.login(user).subscribe((response) => {
      if(response)
        this.router.navigate(["mcd"]);
      else {
        // error handler
       // this.toastr.error("Si è verificato un errore durante il login")
      }
    })      
  }


  onRegister() {
    const user : User = this.formRegister.value;
    this.authService.register(user).subscribe((response) => {
      if(response)
        this.router.navigate(["mcd"]);
      else {
        // error handler
       //  this.toastr.error("L'username inserito è in uso")
      }
    }) 
  }

}
