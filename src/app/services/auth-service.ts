import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { User } from '../models/User';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  user : User;
  constructor(private http: HttpClient) { 
    if(localStorage.getItem('user'))
    JSON.parse(localStorage.getItem('currentUser'))
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.user;
    if (currentUser) {
      // authorised so return true
      return true;
    }
  }

  login(user : User) {
      return this.http.get<any>(`http://cryptoutils.000webhostapp.com/api.php?action=login&username=${user.username}&password=${user.password}`)
          .map(response => {            
            if(response && response.status) {
                this.user = response.user;
                localStorage.setItem('user', JSON.stringify(response.user));
                return true;
            }
            console.log(response)
            return false;
        });
  }

  register(user : User) {
      return this.http.get<any>(`http://cryptoutils.000webhostapp.com/api.php?action=register&username=${user.username}&password=${user.password}&name=${user.name}&surname=${user.surname}`)
          .map(response => {
            if(response && response.status) {
                this.user = user;
                return true;
            }
            console.log(response)
            return false;
        });
  }

  setScore(user : User) {
      return this.http.get<any>(`http://cryptoutils.000webhostapp.com/api.php?action=updateScore&username=${user.username}&score=${user.score}`)
          .map(response => {
            if(response && response.status) {
                this.user = user;
                return true;
            }
            console.log(response)
            return false;
        });
  }    
  
  logout() {
      return null;
  }
}
