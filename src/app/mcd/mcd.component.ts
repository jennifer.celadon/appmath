import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
// import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-mcd',
  templateUrl: './mcd.component.html',
  styleUrls: ['./mcd.component.scss']
})
export class McdComponent implements OnInit {

  constructor(/*private toastr: ToastrService*/) {
    this.init();
   }

  ngOnInit(): void {
  }

  prime_numbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];

  form = new FormGroup({
    val1 :  new FormControl(40,[Validators.required, Validators.min(0)] ),
    val2 :  new FormControl(10,[Validators.required, Validators.min(0)] ),
    mcd :  new FormControl(null,[Validators.required, Validators.min(0)] )
  })
  fact_val1 = [];
  fact_val2 = [];
  mcd_fact = {};
  title = 'math';
  showInfo = false
  showFact = false
  punteggio = 0;

  
  init() {
    const random_1 = Math.floor(Math.random() * (50 - 1)) + 1;
    const random_2 = Math.floor(Math.random() * (100 - random_1)) + random_1;
    this.form.get("val1").setValue(random_1)
    this.form.get("val2").setValue(random_2)
    this.form.get("mcd").setValue(null)
    this.getMcd(this.form.get("val1").value, this.form.get("val2").value)
    this.showInfo = false;
    this.showFact = false;
  }

  showFactorizations() {
    this.showFact = !this.showFact
  }

  submit() {
    let v1 = this.form.get("val1").value;
    let v2 = this.form.get("val2").value;
    let r = 0;
   
    r=v1%v2;
    while(r!=0){
      v1=v2;
      v2=r;
      r=v1%v2;
    }
    
    const mcd = v2;
    if(this.form.get("mcd").value === mcd) {
      // this.toastr.success("Il risultato è corretto! Hai guadagnato 2 punti")
      this.punteggio +=2;
      this.init();
    } else {
      // this.toastr.error("Il risultato è sbagliato! Se vuoi vedere la soluzione premi sul punto di domanda")
      this.punteggio -=1;
      this.showInfo = true;
    }
  }

  getMcd(val1, val2) {

    let fact_val1 = this.getFactorization(val1)
    let fact_val2 = this.getFactorization(val2)
    let mcd_fact = {}
    let keys = Object.keys(fact_val1)
    
    for(let index=0; index < keys.length; index ++ ) {
      let fact = keys[index]
      let exp = fact_val1[keys[index]]
      if(this.fact_val2[fact]){
        let mcd_exp = 1
        if (exp > fact_val2[fact]){
           mcd_exp = fact_val2[fact]
        } else {
          mcd_exp = exp
        }
        mcd_fact[fact] =  mcd_exp
      }
    }

    this.fact_val1 = [];
    this.fact_val2 = [];
    for(let key of keys) {
      this.fact_val1.push({
        value : key, esp : fact_val1[key]
      })
    }
    let keys2 = Object.keys(fact_val2)
    for(let key of keys2) {
      this.fact_val2.push({
        value : key, esp : fact_val2[key]
      })
    }
    this.mcd_fact = mcd_fact
  }

  getFactorization(value) {
   
    const factorization = {}
    let partial = {}
    let index = 0;

    for(index;index < this.prime_numbers.length;index++) {

      while(1) {

        if(value % this.prime_numbers[index] == 0) {
            value /=this.prime_numbers[index];
            if(factorization[this.prime_numbers[index]]) {
              factorization[this.prime_numbers[index]]++
            } else {
              factorization[this.prime_numbers[index]] = 1
            }

        } else {
          break
        }
      }

      if(value === 1) {
        break;
      }
    }

    return  factorization
  }

}