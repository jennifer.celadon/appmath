export interface User {
    username : string;
    password : string;
    name : string| null;
    surname : string| null;
    score : number| null;
}